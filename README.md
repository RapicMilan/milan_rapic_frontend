# Frontend Application

## Installation and Usage
To start using the boilerplate, first install all the dependencies and then run one of the gulp tasks, for example:

 ```
 $ npm i
 $ npm start
 ```

## Npm Tasks

###### Bundle Tasks

Task Name         | Description
----------------- | ---------------------------------------------------------------------
`npm start`       | Generate a development environment with watch and BrowserSync
`npm run dev`     | Same as `npm start`
`npm run prod`    | Compile production code and run unit tests

## Gulp Tasks

###### Bundle Tasks

Task Name         | Description
----------------- | ---------------------------------------------------------------------
`gulp`            | Generate a development environment with watch and browsersync
`gulp prod`       | Compile production code and run unit tests

###### Individual Tasks

Task Name         | Description
----------------- | ----------------------------------------------------
`gulp clean`      | Delete the output directory
`gulp svg`        | Combine svgs into a <symbol> element with paths
`gulp imagemin`   | Minify images and svg files
`gulp minifyHtml` | Inject assets into and compress the main index.html
`gulp move`       | Move source files to output directory
`gulp server`     | Initialise a local server
`gulp styles`     | Compile Sass to CSS
`gulp webpack`    | Bundle js files


## File Structure
The default working directory for development is `src/`. This directory contains all the styles, scripts, fonts and 
images used to create the front-end of the website.

```
dist/
	|- images
	|- css
    |- fonts
	|- js
    |- index.html
src/
|- fonts/
	|- ionicons.eot
	|- ionicons.svg
	|- ionicons.ttf
	|- ionicons.woff
|- images/
	|- profile.jpg
|- js/
	|- components/
	|- app.js
|- sass/
    |- application/
        |- base/
        |- layouts/
        |- modules/
        |- states/
    |- vendor/
        |- ionicons/
    |- application.scss
|- index.html 
```

### Icons
The `src/images/icons/` folder should contain all the svg icons that should be combined to then be injected into 
the page. Have a look at the following links to understand the technique adopted by the boilerplate to make use of 
svg icons:

* https://css-tricks.com/icon-fonts-vs-svg/
* https://sarasoueidan.com/blog/icon-fonts-to-svg/
* https://24ways.org/2014/an-overview-of-svg-sprite-creation-techniques/

(The `<symbol>` element is generated and injected as part of the `minifyHtml` task)

### Images
All images should be placed inside the `src/images/` folder. This is for consistency as opposed to a limitation 
enforced by the `imagemin` task as this task will look for and minify all images inside the `src/` folder that have 
any of the following extensions: `.jpg` `.png` `.gif` `.svg`


