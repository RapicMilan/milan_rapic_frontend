/*
 * @title Tab navigation
 * @description Tab navigation Component
 */

export default class TabNavigation {
    constructor(profile) {
        this.profile = profile;
        this.navigation = [
            { name: 'About',     index: 0, active:true,  renderTemplate:this.renderAboutPage    },
            { name: 'Settings',  index: 1, active:false, renderTemplate:this.renderSettingsPage },
            { name: 'Options 1', index: 2, active:false, renderTemplate:this.renderOption1Page  },
            { name: 'Options 2', index: 3, active:false, renderTemplate:this.renderOption2Page  },
            { name: 'Options 3', index: 4, active:false, renderTemplate:this.renderOption3Page  }
        ];
        this.editAboutForm = false;
    }

    init = () => {
        window.tabChange = this.tabChange;
        this.renderAboutPage();        

        metaQuery.onBreakpointChange( 'desktop', function ( match ) {
            if( match ) {
                // Disable inputs
                $('.text-label[type="text"]').attr('disabled', 'disabled');
            }
        });
    }

     

    updateProfile = (profile) => {
        if (profile.name != undefined) {
            this.profile.name = profile.name;
        }

        if (profile.website != undefined) {
            this.profile.website = profile.website;
        }

        if (profile.phone != undefined) {
            this.profile.phone = profile.phone;
        }

        if (profile.address != undefined) {
            this.profile.address= profile.address;
        }
        
        this.init();
        this.profile.renderProfileInfo();
    };
    
    tabChange = (index) => {
        for (var nav of this.navigation) {
            if (nav.index == index) {
                this.changeActiveTab(nav.index);
                nav.renderTemplate();
                break;
            }
        }
    };

    changeActiveTab = (index) => {
        for (var nav of this.navigation) {
            if (nav.index == index) {
                nav.active = true;
            } else {
                nav.active = false;
            }
        }
    };

    switchEditForm = (event) => {
        if (event != undefined) {
            event.preventDefault();
        }
        
        if (this.editAboutForm) {
            // Close edit
            document.getElementById('about-tab-page').classList.remove('show-form');
            // Disable inputs
            $('.text-label[type="text"]').attr('disabled', 'disabled');
            
            this.editAboutForm = false;
        } else {
            // Show edit
            document.getElementById('about-tab-page').classList.add('show-form');
            $('.text-label[type="text"]').removeAttr('disabled');
            this.editAboutForm = true;
        }
    };

    cancelEditProfile = (event) => {
        event.preventDefault();
        this.switchEditForm(event);
    };

    saveEditProfile = (event) => {    
        event.preventDefault();

        let profile = {
            name: document.getElementById('edit-form-name').value,
            website: document.getElementById('edit-form-website').value,
            phone: document.getElementById('edit-form-phone').value,
            address: document.getElementById('edit-form-address').value,
        };

        this.updateProfile(profile);

        // Save action
        this.switchEditForm(event);
    };

    // Name dialog click event actions
    openNameEditDialog = (event) => {
        event.preventDefault();
        document.getElementById('name-edit-dialog').classList.add('-is-visible');
    };
    closeEditNameDialog = (event) => {
        event.preventDefault();
        document.getElementById('name-edit-dialog').classList.remove('-is-visible');
    };
    updateName = (event) => {
        event.preventDefault();
        let name = document.getElementById('edit-dialog-name').value;
        this.profile.name = name;
        document.getElementById('name-edit-dialog').classList.remove('-is-visible');
        this.init();
        this.profile.renderProfileInfo();
    };

    // Website dialog click event actions
    openWebsiteEditDialog = (event) => {
        event.preventDefault();
        document.getElementById('website-edit-dialog').classList.add('-is-visible');
    };
    closeEditWebsiteDialog = (event) => {
        event.preventDefault();
        document.getElementById('website-edit-dialog').classList.remove('-is-visible');
    };
    updateWebsite = (event) => {
        event.preventDefault();
        let website = document.getElementById('edit-dialog-website').value;
        this.profile.website = website;
        document.getElementById('website-edit-dialog').classList.remove('-is-visible');
        this.init();
        this.profile.renderProfileInfo();
    };


    // Phone dialog click event actions
    openPhoneEditDialog = (event) => {
        event.preventDefault();
        document.getElementById('phone-edit-dialog').classList.add('-is-visible');
    };
    closeEditPhoneDialog = (event) => {
        event.preventDefault();
        document.getElementById('phone-edit-dialog').classList.remove('-is-visible');
    };
    updatePhone = (event) => {
        event.preventDefault();
        let phone = document.getElementById('edit-dialog-phone').value;
        this.profile.phone = phone;
        document.getElementById('phone-edit-dialog').classList.remove('-is-visible');
        this.init();
        this.profile.renderProfileInfo();
    };


    // Address dialog click event actions
    openAddressEditDialog = (event) => {
        event.preventDefault();
        document.getElementById('address-edit-dialog').classList.add('-is-visible');
    };
    closeEditAddressDialog = (event) => {
        event.preventDefault();
        document.getElementById('address-edit-dialog').classList.remove('-is-visible');
    };
    updateAddress = (event) => {
        event.preventDefault();
        let address = document.getElementById('edit-dialog-address').value;
        this.profile.address = address;
        document.getElementById('address-edit-dialog').classList.remove('-is-visible');
        this.init();
        this.profile.renderProfileInfo();
    };

    renderAboutPage = () => {
        let tabNavigationTemplate = `
            ${this.navigation.map(tab => `<div class="tab-navigation__tab ${tab.active==true?'-is-active':''}" onclick="tabChange(${tab.index})">${tab.name}</div>`)}
        `;

        let tabNavigation = `
            <div class="tab-navigation">
                <div class="tab-navigation__container">
                    ${tabNavigationTemplate.replace(/,/g, '')}
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__page">                
                    ${this.profile.renderProfileTabPage()}
                </div>
            </div>
        `;
        document.getElementById('tab-navigation').innerHTML = tabNavigation;
        $('.text-label[type="text"]').attr('disabled', 'disabled');
        this.attachEventListeners();
    };

    renderSettingsPage = () => {
        let tabNavigationTemplate = `
            ${this.navigation.map(tab => `<div class="tab-navigation__tab ${tab.active==true?'-is-active':''}" onclick="tabChange(${tab.index})">${tab.name}</div>`)}
        `;
        let tabNavigation = `
            <div class="tab-navigation">
                <div class="tab-navigation__container">
                    ${tabNavigationTemplate.replace(/,/g, '')}
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__page">
                    <h3>Settings</h3>
                </div>
            </div>
        `;
        document.getElementById('tab-navigation').innerHTML = tabNavigation;
    };

    renderOption1Page = () => {
        let tabNavigationTemplate = `
            ${this.navigation.map(tab => `<div class="tab-navigation__tab ${tab.active==true?'-is-active':''}" onclick="tabChange(${tab.index})">${tab.name}</div>`)}
        `;
        let tabNavigation = `
            <div class="tab-navigation">
                <div class="tab-navigation__container">
                    ${tabNavigationTemplate.replace(/,/g, '')}
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__page">
                    <h3>Options 1</h3>

                </div>
            </div>
        `;
        document.getElementById('tab-navigation').innerHTML = tabNavigation;
    };

    renderOption2Page = () => {
        let tabNavigationTemplate = `
            ${this.navigation.map(tab => `<div class="tab-navigation__tab ${tab.active==true?'-is-active':''}" onclick="tabChange(${tab.index})">${tab.name}</div>`)}
        `;
        let tabNavigation = `
            <div class="tab-navigation">
                <div class="tab-navigation__container">
                    ${tabNavigationTemplate.replace(/,/g, '')}
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__page">
                    <h3>Options 2</h3>
                </div>
            </div>
        `;
        document.getElementById('tab-navigation').innerHTML = tabNavigation;
    };

    renderOption3Page = () => {
        let tabNavigationTemplate = `
            ${this.navigation.map(tab => `<div class="tab-navigation__tab ${tab.active==true?'-is-active':''}" onclick="tabChange(${tab.index})">${tab.name}</div>`)}
        `;
        let tabNavigation = `
            <div class="tab-navigation">
                <div class="tab-navigation__container">
                    ${tabNavigationTemplate.replace(/,/g, '')}
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-content__page">
                    <h3>Options 3</h3>

                </div>
            </div>
        `;
        document.getElementById('tab-navigation').innerHTML = tabNavigation;
    };

    attachEventListeners = () => {
        // Attach event handlers
        let switchFormButton = document.getElementById('switch-form-button');
        if (switchFormButton != undefined) {
            switchFormButton.addEventListener('click', this.switchEditForm);
        }

        let cancelButtonPhone = document.getElementById('form-cancel-button');
        if (cancelButtonPhone != undefined) {
            cancelButtonPhone.addEventListener('click', this.cancelEditProfile);
        }

        let saveButtonPhone = document.getElementById('form-save-button');
        if (saveButtonPhone != undefined) {
            saveButtonPhone.addEventListener('click', this.saveEditProfile);
        }
        

        // Dialogs 

        // Name Edit Dialog    
        let nameEditButtonDialog = document.getElementById('name-edit-button');
        if (nameEditButtonDialog != undefined) {
            nameEditButtonDialog.addEventListener('click', this.openNameEditDialog);
        }
        let nameEditDialogSaveButton = document.getElementById('name-edit-dialog-save-button');
        if (nameEditDialogSaveButton != undefined) {
            nameEditDialogSaveButton.addEventListener('click', this.updateName);
        }
        let nameEditDialogCloseButton = document.getElementById('name-edit-dialog-cancel-button');
        if (nameEditDialogCloseButton != undefined) {
            nameEditDialogCloseButton.addEventListener('click', this.closeEditNameDialog);
        }



         // Website Edit Dialog   
        let websiteEditButtonDialog = document.getElementById('website-edit-button');
        if (websiteEditButtonDialog != undefined) {
            websiteEditButtonDialog.addEventListener('click', this.openWebsiteEditDialog);
        }
        let websiteEditDialogSaveButton = document.getElementById('website-edit-dialog-save-button');
        if (websiteEditDialogSaveButton != undefined) {
            websiteEditDialogSaveButton.addEventListener('click', this.updateWebsite);
        }
        let websiteEditDialogCloseButton = document.getElementById('website-edit-dialog-cancel-button');
        if (websiteEditDialogCloseButton != undefined) {
            websiteEditDialogCloseButton.addEventListener('click', this.closeEditWebsiteDialog);
        }

         // Phone Edit Dialog   
        let phoneEditButtonDialog = document.getElementById('phone-edit-button');
        if (phoneEditButtonDialog != undefined) {
            phoneEditButtonDialog.addEventListener('click', this.openPhoneEditDialog);
        }
        let phoneEditDialogSaveButton = document.getElementById('phone-edit-dialog-save-button');
        if (phoneEditDialogSaveButton != undefined) {
            phoneEditDialogSaveButton.addEventListener('click', this.updatePhone);
        }
        let phoneEditDialogCloseButton = document.getElementById('phone-edit-dialog-cancel-button');
        if (phoneEditDialogCloseButton != undefined) {
            phoneEditDialogCloseButton.addEventListener('click', this.closeEditPhoneDialog);
        }

         // Address Edit Dialog   
        let addressEditButtonDialog = document.getElementById('address-edit-button');
        if (addressEditButtonDialog != undefined) {
            addressEditButtonDialog.addEventListener('click', this.openAddressEditDialog);
        }
        let addressEditDialogSaveButton = document.getElementById('address-edit-dialog-save-button');
        if (addressEditDialogSaveButton != undefined) {
            addressEditDialogSaveButton.addEventListener('click', this.updateAddress);
        }
        let addressEditDialogCloseButton = document.getElementById('address-edit-dialog-cancel-button');
        if (addressEditDialogCloseButton != undefined) {
            addressEditDialogCloseButton.addEventListener('click', this.closeEditAddressDialog);
        }
    }

}










