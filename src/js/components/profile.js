/*
 * @title Profile
 * @description Profile Component
 */


export default class Profile {
    constructor (name, website, phone, address) {
        this.name = name;
        this.website = website;
        this.phone = phone;
        this.address = address;
        this.switchEditForm = this.switchEditForm;
    }

    renderProfileInfo = () => {
        let profileinfoTemplate = `
            <div class="profile__info">
                <h1>${this.name}</h1>
                <div class="profile__item">
                    <span class="ion-ios-telephone-outline profile__item__icon"></span>
                    ${this.phone}
                </div>
                <div class="profile__item">
                    <span class="ion-ios-location-outline profile__item__icon"></span>
                    ${this.address}
                </div>
            </div>
        `;
        document.getElementById('profile-info').innerHTML = profileinfoTemplate;
    };


    renderProfileTabPage = () => {
        let template = `
            <div id="about-tab-page">
                <div class="page-title">
                    <h3>ABOUT</h3>
                </div>
                <div class="page-action">
                    ${this.renderFormButtons()}
                </div>
                <div class="edit-form">
                    <form>
                        ${this.renderName()}   
                        ${this.renderWebsite()} 
                        ${this.renderPhone()} 
                        ${this.renderAddress()} 
                    </form>
                </div>
            </div>
        `;
        return template;
    };

    renderFormButtons = () => {
        return `
            <button id="switch-form-button" type="button" class="page-action__button form-button">
                <span class="ion-edit profile__item__icon"></span>
            </button>
            <button id="form-cancel-button" type="button" class="button--flat form-edit-button">Cancel</button>
            <button id="form-save-button" type="button" class="button--flat form-edit-button">Save</button>
        `;
    };

    renderName = () => {
        return `
            <div class="edit-form__group">    
                <div class="edit-form__group-item">  
                    <input id="edit-form-name" type="text" class="text-input name text-label" required value='${this.name}'>
                    <span class="text-input__highlight"></span>
                    <span class="text-input__bar"></span>
                    <label class="text-input__label">Name</label>
                </div>
                <button id="name-edit-button" type="button" class="edit-form__group__button">
                    <span class="ion-edit profile__item__icon"></span>
                </button>
                <div id="name-edit-dialog" class="edit-form__dialog">
                    <div class="edit-form__dialog__containter">
                        <form>
                            <div class="edit-form__group">      
                                <input id="edit-dialog-name" type="text" class="text-input" required value='${this.name}'>
                                <span class="text-input__highlight"></span>
                                <span class="text-input__bar"></span>
                                <label class="text-input__label">Name</label>
                            </div>
                            <div class="edit-form__dialog__buttons">
                                <button id="name-edit-dialog-save-button" type="button" class="button--raised--fill">Save</button>
                                <button id="name-edit-dialog-cancel-button" type="button" class="button--raised">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        `;
    };

    renderWebsite = () => {
        return `
            <div class="edit-form__group">
                <div class="edit-form__group-item"> 
                    <span class="ion-android-globe form-icon"></span> 
                    <input id="edit-form-website" type="text" class="text-input text-label" required value='${this.website}'>
                    <span class="text-input__highlight"></span>
                    <span class="text-input__bar"></span>
                    <label class="text-input__label">Website</label>
                </div>
                <button id="website-edit-button" type="button" class="edit-form__group__button">
                    <span class="ion-edit profile__item__icon"></span>
                </button>
                <div id="website-edit-dialog" class="edit-form__dialog">
                    <div class="edit-form__dialog__containter">
                        <form>
                            <div class="edit-form__group">      
                                <input id="edit-dialog-website" type="text" class="text-input" required value='${this.website}'>
                                <span class="text-input__highlight"></span>
                                <span class="text-input__bar"></span>
                                <label class="text-input__label">Website</label>
                            </div>
                            <div class="edit-form__dialog__buttons">
                                <button id="website-edit-dialog-save-button" type="button" class="button--raised--fill">Save</button>
                                <button id="website-edit-dialog-cancel-button" type="button" class="button--raised">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        `;
    };

    
    renderPhone = () => {
        return `
            <div class="edit-form__group">
                <div class="edit-form__group-item"> 
                    <span class="ion-ios-telephone-outline form-icon"></span>
                    <input id="edit-form-phone" type="text" class="text-input text-label" required value='${this.phone}'>
                    <span class="text-input__highlight"></span>
                    <span class="text-input__bar"></span>
                    <label class="text-input__label">Phone</label>
                </div>

                <button id="phone-edit-button" type="button" class="edit-form__group__button">
                    <span class="ion-edit profile__item__icon"></span>
                </button>
                <div id="phone-edit-dialog" class="edit-form__dialog">
                    <div class="edit-form__dialog__containter">
                        <form>
                            <div class="edit-form__group">      
                                <input id="edit-dialog-phone" type="text" class="text-input" required value='${this.phone}'>
                                <span class="text-input__highlight"></span>
                                <span class="text-input__bar"></span>
                                <label class="text-input__label">Phone</label>
                            </div>
                            <div class="edit-form__dialog__buttons">
                                <button id="phone-edit-dialog-save-button" type="button" class="button--raised--fill">Save</button>
                                <button id="phone-edit-dialog-cancel-button" type="button" class="button--raised">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        `;
    };

    renderAddress = () => {
        return `
            <div class="edit-form__group">     
                <div class="edit-form__group-item">  
                    <span class="ion-ios-location-outline form-icon"></span>
                    <input id="edit-form-address" type="text" class="text-input text-label" required value='${this.address}'>
                    <span class="text-input__highlight"></span>
                    <span class="text-input__bar"></span>
                    <label class="text-input__label">Address</label>
                </div>
                <button id="address-edit-button" type="button" class="edit-form__group__button">
                    <span class="ion-edit profile__item__icon"></span>
                </button>
                <div id="address-edit-dialog" class="edit-form__dialog">
                    <div class="edit-form__dialog__containter">
                        <form>
                            <div class="edit-form__group">      
                                <input id="edit-dialog-address" type="text" class="text-input" required value='${this.address}'>
                                <span class="text-input__highlight"></span>
                                <span class="text-input__bar"></span>
                                <label class="text-input__label">Address</label>
                            </div>
                            <div class="edit-form__dialog__buttons">
                                <button id="address-edit-dialog-save-button" type="button" class="button--raised--fill">Save</button>
                                <button id="address-edit-dialog-cancel-button" type="button" class="button--raised">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        `;
    };

}
