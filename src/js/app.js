/*
 * @title App
 * @description Application entry point
 */


import TabNavigation from './components/tab-navigation.js';
import Profile from './components/profile.js';

const defaultProfile = {
    name: 'Jessica Parker',
    website: 'www.seller.com',
    phone: '(949)325-68594',
    address: 'Newport Beach CA'
};

const init = () => {
    let profile = new Profile(defaultProfile.name, 
                              defaultProfile.website, 
                              defaultProfile.phone, 
                              defaultProfile.address);
    
    let tabNav = new TabNavigation(profile);

    profile.renderProfileInfo();
    tabNav.init();
};

init();